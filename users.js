// Importacion de bcrypt
console.log("Entro a encriptarPassAsync()");
const bcrypt = require('bcrypt');
const saltRounds = 10;
const textoPass = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

bcrypt.genSalt(saltRounds, function(err, salt){
  bcrypt.hash(textoPass, salt, function(err, hash){
    console.log("bcrypt: " + bcrypt);
  });
});

/* Sync */
/*
var salt = bcrypt.genSaltSync(saltRounds);
var hash = bcrypt.hashSync(textPass, salt);
console.log("hash: " + hash);
*/
