//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + '/build/default'));
var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.listen(port);

console.log('Ejecutando Polymer desde node ' + port);

app.get('/', function (req,res) {
  //res.send('Hola Mundo nodejs');
  res.sendFile("index.html",{root:'.'});
});
